package com.server.auth_deer.enums;

public enum TokenEnum {

	TOKEN_TYPE("Bearer"), 
	GRANT_TYPE_PASSWORD("password"), 
	GRANT_TYPE_CLIENT_CREDENTIAL("client_credentials"),
	EXPIRES_IN("60000"), 
	REFRESH_EXPIRES_IN("180000"),
	TOKEN_STATUS("1");

	private String name;

	private TokenEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public int getInt() {
		return Integer.parseInt(name);
	}

	public boolean isEquals(String other) {
		return this.name.equals(other);
	}

}
