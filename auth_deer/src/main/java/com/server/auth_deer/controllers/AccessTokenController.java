package com.server.auth_deer.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.server.auth_deer.entities.SysAccessTokenEntity;
import com.server.auth_deer.exception.InternalServerErrorException;
import com.server.auth_deer.exception.UnauthorizedException;
import com.server.auth_deer.exception.UnprocessableEntityException;
import com.server.auth_deer.model.token.ResponseAccessTokenModel;
import com.server.auth_deer.services.SysAccessTokenService;

@RestController
@RequestMapping("/api/oauth2/access-token")
public class AccessTokenController {

	@Autowired
	private SysAccessTokenService tokenService;

	@PostMapping("/verifyAndRefreshToken")
	public ResponseEntity<ResponseAccessTokenModel> verifyAndRefreshToken(@RequestHeader HttpHeaders headers) {
		try {
			String reqToken = validateRequestHeaderAuthentication(headers);
			ResponseAccessTokenModel validatedToken = tokenService.validateTokenExpires(reqToken);

			if (validatedToken.isExpiry()) {
				SysAccessTokenEntity refreshedToken = tokenService.refreshToken(validatedToken.getAccess_token());
				validatedToken.setAccess_token(refreshedToken.getAccessToken());
			}
			return new ResponseEntity<>(validatedToken, HttpStatus.OK);
		} catch (UnprocessableEntityException e) {
			throw new UnprocessableEntityException(e.getMessage());
		} catch (UnauthorizedException e) {
			throw new UnauthorizedException(e.getMessage());
		} catch (Exception e) {
			throw new InternalServerErrorException(e.getMessage());
		}
	}

	private String validateRequestHeaderAuthentication(HttpHeaders reqHeader) {
		String authorizationRequestHeader = reqHeader.getFirst(HttpHeaders.AUTHORIZATION);
		if (null == authorizationRequestHeader || "".equals(authorizationRequestHeader)) {
			throw new UnprocessableEntityException("missing Authorization request header");
		} // token = [0]Bearer [1]token
		String token = authorizationRequestHeader.split(" ")[1];
		return token;
	}

}
