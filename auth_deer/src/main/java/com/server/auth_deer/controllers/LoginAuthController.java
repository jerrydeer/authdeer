package com.server.auth_deer.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.server.auth_deer.entities.SysAccessTokenEntity;
import com.server.auth_deer.exception.InternalServerErrorException;
import com.server.auth_deer.exception.UnauthorizedException;
import com.server.auth_deer.exception.UnprocessableEntityException;
import com.server.auth_deer.model.token.ReqParamsAccessToken;
import com.server.auth_deer.model.token.ResponseAccessTokenModel;
import com.server.auth_deer.services.LoginAuthService;

@RestController
@RequestMapping("/api/oauth2/login")
public class LoginAuthController {

	@Autowired
	private LoginAuthService loginAuthService;

	@PostMapping("/accessToken")
	public ResponseEntity<?> accessToken(@RequestParam MultiValueMap<String, String> reqBody) {
		try {
			ReqParamsAccessToken accessTokenReqParams = validateAccessTokenReqParams(reqBody);

			SysAccessTokenEntity tokenEntity = loginAuthService.validateClientCredential(accessTokenReqParams);
			SysAccessTokenEntity createdToken = loginAuthService.createToken(tokenEntity);

			return new ResponseEntity<>(buildResponseAccessToken(createdToken), HttpStatus.OK);
		} catch (UnprocessableEntityException e) {
			throw new UnprocessableEntityException(e.getMessage());
		} catch (UnauthorizedException e) {
			throw new UnauthorizedException(e.getMessage());
		} catch (Exception e) {
			throw new InternalServerErrorException(e.getMessage());
		}
	}

	// ----------------------------------------------------------------------------------------------------------

	private ReqParamsAccessToken validateAccessTokenReqParams(MultiValueMap<String, String> reqBody) throws Exception {
		ReqParamsAccessToken response = new ReqParamsAccessToken();
		try {
			String username = null, password = null;
			String grantType = checkNullOrBlank("grant_type", reqBody.getFirst("grant_type"));
			String clientId = checkNullOrBlank("client_id", reqBody.getFirst("client_id"));
			String clientSecret = checkNullOrBlank("client_secret", reqBody.getFirst("client_secret"));
			String scope = checkNullOrBlank("scope", reqBody.getFirst("scope"));

			if ("password".equals(grantType)) {
				username = checkNullOrBlank("username", reqBody.getFirst("username"));
				password = checkNullOrBlank("password", reqBody.getFirst("password"));
			} else if ("client_credentials".equals(grantType)) {

			} else {
				throw new Exception("Grant type is not support");
			}
			response.setClientId(clientId);
			response.setClientSecret(clientSecret);
			response.setGrantType(grantType);
			response.setUsername(username);
			response.setPassword(password);
			response.setScope(scope);
			return response;
		} catch (Exception e) {
			throw new UnprocessableEntityException(e.getMessage());
		}
	}

	private ResponseAccessTokenModel buildResponseAccessToken(SysAccessTokenEntity accessTokenEntity) {
		ResponseAccessTokenModel responseAccessToken = new ResponseAccessTokenModel();
		responseAccessToken.setAccess_token(accessTokenEntity.getAccessToken());
		responseAccessToken.setRefresh_token(accessTokenEntity.getRefreshToken());
		responseAccessToken.setExpires_in(accessTokenEntity.getExpiresIn());
		responseAccessToken.setToken_type(accessTokenEntity.getTokenType());
		responseAccessToken.setScope(accessTokenEntity.getScope());
		responseAccessToken.setCreate_by(accessTokenEntity.getCreateBy());
		return responseAccessToken;
	}

	private String checkNullOrBlank(String k, String v) throws Exception {
		if (null == v || "".equals(v)) {
			throw new Exception(k + " query param is null or empty");
		}
		return v;
	}

}
