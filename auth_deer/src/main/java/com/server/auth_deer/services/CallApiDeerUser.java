package com.server.auth_deer.services;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.server.auth_deer.model.UsernamePasswordDTO;

@Service
public class CallApiDeerUser {

	public UsernamePasswordDTO validateCredentialUser(String callbackHost, String username, String password) {
		UsernamePasswordDTO res = new UsernamePasswordDTO();
		Gson gson = new Gson();

		String url = callbackHost;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		UsernamePasswordDTO reqDto = new UsernamePasswordDTO();
		reqDto.setUsername(username);
		reqDto.setPassword(password);
		String jsonReqDto = gson.toJson(reqDto);

		HttpEntity<String> request = new HttpEntity<String>(jsonReqDto, headers);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<UsernamePasswordDTO> response = restTemplate.postForEntity(url, request,
				UsernamePasswordDTO.class);

		res.setUsername(response.getBody().getUsername());
		return res;
	}
}
