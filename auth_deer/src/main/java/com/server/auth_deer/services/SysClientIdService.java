package com.server.auth_deer.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.server.auth_deer.entities.SysClientIdEntity;
import com.server.auth_deer.repositories.SysClientIdRepository;

@Service
public class SysClientIdService {

	@Autowired
	private SysClientIdRepository clientIdRepo;
	
	public SysClientIdEntity findByClientIdAndSecret(String clientId, String clientSecret) {
		return clientIdRepo.findByClientIdAndSecret(clientId, clientSecret).get(0);
	}
	
}
