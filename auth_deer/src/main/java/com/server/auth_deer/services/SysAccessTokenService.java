package com.server.auth_deer.services;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.server.auth_deer.entities.SysAccessTokenEntity;
import com.server.auth_deer.enums.TokenEnum;
import com.server.auth_deer.exception.UnauthorizedException;
import com.server.auth_deer.model.token.ResponseAccessTokenModel;
import com.server.auth_deer.repositories.SysAccessTokenRepository;

@Service
public class SysAccessTokenService {

	@Autowired
	private SysAccessTokenRepository tokenRepo;

	public ResponseAccessTokenModel validateTokenExpires(String reqAccessToken) {
		List<Object[]> listSysAccessToken = tokenRepo.validateTokenExpires(reqAccessToken);
		if (listSysAccessToken.size() > 0) {
			Object[] sysAccessToken = listSysAccessToken.get(0);
			
			return setResVerifyAccessTokenDTO(sysAccessToken);
		}
		throw new UnauthorizedException("refresh Token expired or not found.");
	}
	
	public SysAccessTokenEntity refreshToken(String token) {
		Optional<SysAccessTokenEntity> getTokenEntityByToken = getTokenEntityByToken(token);
		SysAccessTokenEntity oldSysAccessTokenEntity = getTokenEntityByToken.get();

		SysAccessTokenEntity newSysAccessTokenEntity = new SysAccessTokenEntity();
		newSysAccessTokenEntity.setAccessToken("" + UUID.randomUUID());
		newSysAccessTokenEntity.setAuthorizeCode(oldSysAccessTokenEntity.getAuthorizeCode());
		newSysAccessTokenEntity.setClientId(oldSysAccessTokenEntity.getClientId());
		newSysAccessTokenEntity.setCreateTime(new Date());
		newSysAccessTokenEntity.setUpdateTime(new Date());
		newSysAccessTokenEntity.setExpiresIn(TokenEnum.EXPIRES_IN.getInt());
		newSysAccessTokenEntity.setScope(oldSysAccessTokenEntity.getScope());
		newSysAccessTokenEntity.setTokenStatus(TokenEnum.TOKEN_STATUS.getInt());
		newSysAccessTokenEntity.setTokenType(TokenEnum.TOKEN_TYPE.getName());
		newSysAccessTokenEntity.setRefreshExpires(TokenEnum.REFRESH_EXPIRES_IN.getInt());
		newSysAccessTokenEntity.setRefreshToken(oldSysAccessTokenEntity.getRefreshToken());
		newSysAccessTokenEntity.setCreateBy(oldSysAccessTokenEntity.getCreateBy());

		tokenRepo.delete(oldSysAccessTokenEntity);
		SysAccessTokenEntity save = tokenRepo.save(newSysAccessTokenEntity);
		return save;
	}

	public Integer deleteRefreshExpiresToken() {
		try {
			return tokenRepo.deleteRefreshExpiresToken();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}
	
	public Optional<SysAccessTokenEntity> getTokenEntityByToken(String token) {
		Optional<SysAccessTokenEntity> findById = tokenRepo.findById(token);
		return findById;
	}

	public SysAccessTokenEntity createToken(SysAccessTokenEntity tokenEntity) throws Exception {
		SysAccessTokenEntity save = tokenRepo.save(tokenEntity);
		return save;
	}

	public SysAccessTokenEntity buildTokenEntity(String clientId, String createBy, String scope) {
		SysAccessTokenEntity tokenEntity = new SysAccessTokenEntity();
		tokenEntity.setAccessToken("" + UUID.randomUUID());
		tokenEntity.setAuthorizeCode("" + UUID.randomUUID());
		tokenEntity.setClientId(clientId);
		tokenEntity.setCreateTime(new Date());
		tokenEntity.setExpiresIn(TokenEnum.EXPIRES_IN.getInt());
		tokenEntity.setScope(scope);
		tokenEntity.setTokenStatus(TokenEnum.TOKEN_STATUS.getInt());
		tokenEntity.setTokenType(TokenEnum.TOKEN_TYPE.getName());
		tokenEntity.setRefreshExpires(TokenEnum.REFRESH_EXPIRES_IN.getInt());
		tokenEntity.setRefreshToken("" + UUID.randomUUID());
		tokenEntity.setCreateBy(createBy);
		return tokenEntity;
	}

	private ResponseAccessTokenModel setResVerifyAccessTokenDTO(Object[] sysAccessToken) {
		BigInteger expiry = (BigInteger) sysAccessToken[0];
		
		boolean isTokenExpires = expiry.compareTo(BigInteger.ZERO) > 0;
		String accessToken = (String) sysAccessToken[1];
		String refreshToken = (String) sysAccessToken[2];
		String tokenType = (String) sysAccessToken[3];
		BigInteger expiresIn = (BigInteger) sysAccessToken[4];
		String scope = (String) sysAccessToken[5];
		String createBy = (String) sysAccessToken[6];
		
		ResponseAccessTokenModel res = new ResponseAccessTokenModel();
		res.setAccess_token(accessToken);
		res.setExpiry(isTokenExpires);
		res.setRefresh_token(refreshToken);
		res.setToken_type(tokenType);
		res.setExpires_in(expiresIn.longValue());
		res.setScope(scope);
		res.setCreate_by(createBy);
		return res;
	}

}
