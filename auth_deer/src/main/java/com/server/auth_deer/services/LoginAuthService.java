package com.server.auth_deer.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.server.auth_deer.entities.SysAccessTokenEntity;
import com.server.auth_deer.entities.SysClientIdEntity;
import com.server.auth_deer.enums.TokenEnum;
import com.server.auth_deer.exception.UnauthorizedException;
import com.server.auth_deer.model.UsernamePasswordDTO;
import com.server.auth_deer.model.token.ReqParamsAccessToken;

@Service
public class LoginAuthService {

	@Autowired
	private CallApiDeerUser callApiDeerUser;
	@Autowired
	private SysAccessTokenService tokenService;
	@Autowired
	private SysClientIdService clientIdService;

	public SysAccessTokenEntity validateClientCredential(ReqParamsAccessToken reqAccessToken) throws Exception {
		tokenService.deleteRefreshExpiresToken();
		String reqGrantType = reqAccessToken.getGrantType();
		String reqUsername = reqAccessToken.getUsername();
		String reqPassword = reqAccessToken.getPassword();
		String reqClientId = reqAccessToken.getClientId();
		String reqClientSecret = reqAccessToken.getClientSecret();
		String reqScope = reqAccessToken.getScope();

		SysClientIdEntity clientIdEntity = clientIdService.findByClientIdAndSecret(reqClientId, reqClientSecret);
		if (null == clientIdEntity) {
			throw new UnauthorizedException("Client credential invalid");
		}

		String createBy = null;
		if (TokenEnum.GRANT_TYPE_PASSWORD.isEquals(reqGrantType)) {
			String callbackHost = clientIdEntity.getCallbackHost();
			createBy = validateClientCredentialTypePassword(callbackHost, reqUsername, reqPassword);

		} else if (TokenEnum.GRANT_TYPE_CLIENT_CREDENTIAL.isEquals(reqGrantType)) {
			createBy = reqClientId;
		}
		return tokenService.buildTokenEntity(reqClientId, createBy, reqScope);
	}

	public SysAccessTokenEntity createToken(SysAccessTokenEntity tokenEntity) throws Exception {
		SysAccessTokenEntity createdToken = tokenService.createToken(tokenEntity);
		return createdToken;
	}

	// ---------------------------------------------------------------------------------------------------------
	private String validateClientCredentialTypePassword(String callbackHost, String username, String password)
			throws Exception {
		UsernamePasswordDTO validate = callApiDeerUser.validateCredentialUser(callbackHost, username, password);
		if (validate != null && validate.getUsername() != null && !"".equals(validate.getUsername())) {
			return username;
		}
		throw new UnauthorizedException("Client credential invalid");
	}

}
