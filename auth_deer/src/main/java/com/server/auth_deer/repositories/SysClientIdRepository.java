package com.server.auth_deer.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.server.auth_deer.entities.SysClientIdEntity;

@Repository
public interface SysClientIdRepository extends JpaRepository<SysClientIdEntity, String> {

	public List<SysClientIdEntity> findByClientIdAndSecret(String clientId, String secret);
	
}
