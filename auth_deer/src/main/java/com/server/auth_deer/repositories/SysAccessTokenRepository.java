package com.server.auth_deer.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.server.auth_deer.entities.SysAccessTokenEntity;

@Repository
public interface SysAccessTokenRepository extends JpaRepository<SysAccessTokenEntity, String> {

	@Modifying
	@Transactional
	@Query(value = "DELETE FROM sys_access_token WHERE DATE_ADD(create_time, INTERVAL (refresh_expires /1000) SECOND) <= (DATE_ADD(now(), INTERVAL 7 HOUR))", nativeQuery = true)
	public Integer deleteRefreshExpiresToken();
	
	@Transactional
	@Query(value = "SELECT DATE_ADD(s.create_time, INTERVAL (expires_in/1000) SECOND) <= (DATE_ADD(now(), INTERVAL 7 HOUR)) AS expiry, "
			+ "s.* FROM sys_access_token s "
			+ "WHERE access_token = :token "
			+ "AND (DATE_ADD(s.create_time, INTERVAL (s.refresh_expires /1000) SECOND)) > (DATE_ADD(now(), INTERVAL 7 HOUR));", nativeQuery = true)
	public List<Object[]> validateTokenExpires(String token);

}
