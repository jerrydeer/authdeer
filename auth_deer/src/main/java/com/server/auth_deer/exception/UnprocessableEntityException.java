package com.server.auth_deer.exception;

public class UnprocessableEntityException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnprocessableEntityException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	
}
