package com.server.auth_deer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

@Component
public class CrossSiteInterceptor implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {
		if (!response.containsHeader("Access-Control-Allow-Origin")) {
			String origin = request.getHeader("origin");
			response.addHeader("Access-Control-Allow-Origin", origin);
		}
		if (!response.containsHeader("Access-Control-Allow-Headers")) {
			response.addHeader("Access-Control-Allow-Headers", "Authorization, Token, Content-Type");
		}
		if (!response.containsHeader("Access-Control-Allow-Methods")) {
			response.addHeader("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
		}
		if (!response.containsHeader("X-Content-Type-Options")) {
			response.addHeader("X-Content-Type-Options", "X-Content-Type-Options");
		}
		if (request.getMethod().toLowerCase().equals("options")) {
			response.setStatus(200);
			response.getOutputStream().println("Options accepted");
			return false;
		}
		return true;
	}

}
