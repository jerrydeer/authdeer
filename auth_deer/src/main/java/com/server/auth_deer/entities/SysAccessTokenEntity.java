package com.server.auth_deer.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="sys_access_token")
public class SysAccessTokenEntity {
	
//    @Transient
//    private Integer age;
    
    @Id
    @Column(name="access_token")
    private String accessToken;
    
    @Column(name="refresh_token")
    private String refreshToken;
    
    @Column(name="token_type")
    private String tokenType;
    
    @Column(name="expires_in")
    private long expiresIn;
    
    private String scope;
    
    @Column(name="create_by")
    private String createBy;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="create_time")
    private Date createTime;
    
    @Column(name="authorize_code")
    private String authorizeCode;
    
    @Column(name="refresh_expires")
    private int refreshExpires;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="update_time")
    private Date updateTime;
    
    @Column(name="token_status")
    private int tokenStatus;
    
    @Column(name="client_id")
    private String clientId;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public long getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(long expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAuthorizeCode() {
		return authorizeCode;
	}

	public void setAuthorizeCode(String authorizeCode) {
		this.authorizeCode = authorizeCode;
	}

	public int getRefreshExpires() {
		return refreshExpires;
	}

	public void setRefreshExpires(int refreshExpires) {
		this.refreshExpires = refreshExpires;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public int getTokenStatus() {
		return tokenStatus;
	}

	public void setTokenStatus(int tokenStatus) {
		this.tokenStatus = tokenStatus;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
}
