package com.server.auth_deer.entities.custom;

import java.util.Date;

public class SysAccessTokenNative {

	private String access_token;
	private String refresh_token;
	private String token_type;
	private int expires_in;
	private String scope;
	private String create_by;
	private Date create_time;
	private String authorize_code;
	private int refresh_expires;
	private Date update_time;
	private int token_status;
	private String client_id;
	private String expiry;

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public String getRefresh_token() {
		return refresh_token;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}

	public String getToken_type() {
		return token_type;
	}

	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}

	public int getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(int expires_in) {
		this.expires_in = expires_in;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getCreate_by() {
		return create_by;
	}

	public void setCreate_by(String create_by) {
		this.create_by = create_by;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public String getAuthorize_code() {
		return authorize_code;
	}

	public void setAuthorize_code(String authorize_code) {
		this.authorize_code = authorize_code;
	}

	public int getRefresh_expires() {
		return refresh_expires;
	}

	public void setRefresh_expires(int refresh_expires) {
		this.refresh_expires = refresh_expires;
	}

	public Date getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}

	public int getToken_status() {
		return token_status;
	}

	public void setToken_status(int token_status) {
		this.token_status = token_status;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getExpiry() {
		return expiry;
	}

	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}

}
