package com.server.auth_deer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthDeerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthDeerApplication.class, args);
	}

}
