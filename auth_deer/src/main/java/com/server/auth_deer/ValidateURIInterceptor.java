package com.server.auth_deer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.server.auth_deer.exception.UnprocessableEntityException;
import com.server.auth_deer.model.HierarchyUriModel;

@Component
public class ValidateURIInterceptor implements HandlerInterceptor {
	Logger log = org.slf4j.LoggerFactory.getLogger(this.getClass());

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {
		try {
			String requestUri = request.getRequestURI();
			String contextPath = request.getServletContext().getContextPath();
			String uriWithoutContextPath = requestUri.substring(contextPath.length());
			HierarchyUriModel hierarchyUri = mapRequestUriModel(uriWithoutContextPath);
			
			if (hierarchyUri.isHaveLoginOauth()) {	// do generate Token
				System.out.println("do generate Token in Controller Service");
			}
			
			if (hierarchyUri.isHaveUser()) { // do validate Token
				String token = request.getHeader("Authorization");
				if (null == token || "".equals(token)) {
					throw new Exception("missing request header Authorization");
				}
			}
		} catch (Exception e) {
			throw new UnprocessableEntityException(e.getMessage());
		}
		log.info("preHandle is complete");
		return true;
	}

	private HierarchyUriModel mapRequestUriModel(String uriWithoutContextPath) {
		String[] splitUriWithoutContextPath = uriWithoutContextPath.split("/");
		HierarchyUriModel requestUriModel = new HierarchyUriModel();
		for (String hierarchyUri : splitUriWithoutContextPath) {
			if ("api".equals(hierarchyUri)) {
				requestUriModel.setHaveApi(true);
			}
			if ("oauth2".equals(hierarchyUri)) {
				requestUriModel.setHaveLoginOauth(true);
			}
			if ("user".equals(hierarchyUri)) {
				requestUriModel.setHaveUser(true);
			}
		}
		return requestUriModel;
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object object, Exception arg3)
			throws Exception {
		log.info("Request is complete");
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object object, ModelAndView model)
			throws Exception {
		log.info("Handler execution is complete");
	}

}
