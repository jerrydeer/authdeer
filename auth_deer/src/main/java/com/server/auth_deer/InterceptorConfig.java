package com.server.auth_deer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SuppressWarnings("deprecation")
@Configuration
public class InterceptorConfig extends WebMvcConfigurerAdapter {

	@Autowired
	CrossSiteInterceptor crossSiteInterceptor;
	@Autowired
	ValidateURIInterceptor validateURIInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(validateURIInterceptor);
		registry.addInterceptor(crossSiteInterceptor);
	}
}
