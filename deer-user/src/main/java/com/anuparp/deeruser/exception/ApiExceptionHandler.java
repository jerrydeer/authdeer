package com.anuparp.deeruser.exception;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler {

	@ExceptionHandler(value = { InternalServerErrorException.class })
	public ResponseEntity<Object> handleInternalServerErrorException(InternalServerErrorException e) {
		HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		ApiExceptionModel apiException = new ApiExceptionModel(e.getMessage(), ZonedDateTime.now(ZoneId.of("Z")),
				httpStatus, 500);
		return new ResponseEntity<>(apiException, httpStatus);
	}

	@ExceptionHandler(value = { UnprocessableEntityException.class })
	public ResponseEntity<Object> handleUnprocessableEntityException(UnprocessableEntityException e) {
		HttpStatus httpStatus = HttpStatus.UNPROCESSABLE_ENTITY;
		ApiExceptionModel apiException = new ApiExceptionModel(e.getMessage(), ZonedDateTime.now(ZoneId.of("Z")),
				httpStatus, 422);
		return new ResponseEntity<>(apiException, httpStatus);
	}

	@ExceptionHandler(value = { UnauthorizedException.class })
	public ResponseEntity<Object> handleUnauthorizedException(UnauthorizedException e) {
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
		ApiExceptionModel apiException = new ApiExceptionModel(e.getMessage(), ZonedDateTime.now(ZoneId.of("Z")),
				httpStatus, 401);
		return new ResponseEntity<>(apiException, httpStatus);
	}

}
