package com.anuparp.deeruser.exception;

import java.time.ZonedDateTime;

import org.springframework.http.HttpStatus;

public class ApiExceptionModel {

	private String message;
	private ZonedDateTime timestamp;
	private HttpStatus error;
	private int status;

	public ApiExceptionModel(String message, ZonedDateTime timestamp, HttpStatus error, int status) {
		super();
		this.message = message;
		this.timestamp = timestamp;
		this.error = error;
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public ZonedDateTime getTimestamp() {
		return timestamp;
	}

	public HttpStatus getError() {
		return error;
	}

	public int getStatus() {
		return status;
	}

}
