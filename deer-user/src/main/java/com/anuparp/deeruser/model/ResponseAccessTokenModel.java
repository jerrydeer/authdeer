package com.anuparp.deeruser.model;

public class ResponseAccessTokenModel {

	private String access_token;
	private String token_type;
	private String refresh_token;
	private long expires_in;
	private String scope;
	private String create_by;
	private boolean expiry;
	
	public String getAccess_token() {
		return access_token;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	public String getToken_type() {
		return token_type;
	}
	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}
	public String getRefresh_token() {
		return refresh_token;
	}
	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}
	public long getExpires_in() {
		return expires_in;
	}
	public void setExpires_in(long expires_in) {
		this.expires_in = expires_in;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public String getCreate_by() {
		return create_by;
	}
	public void setCreate_by(String create_by) {
		this.create_by = create_by;
	}
	public boolean isExpiry() {
		return expiry;
	}
	public void setExpiry(boolean expiry) {
		this.expiry = expiry;
	}
	
}
