package com.anuparp.deeruser.model;

public class HierarchyUriModel {

	private boolean haveApi;
	private boolean haveLoginOauth;
	private boolean haveAdmin;
	private boolean haveUser;
	
	public boolean isHaveApi() {
		return haveApi;
	}
	public void setHaveApi(boolean haveApi) {
		this.haveApi = haveApi;
	}
	public boolean isHaveLoginOauth() {
		return haveLoginOauth;
	}
	public void setHaveLoginOauth(boolean haveLoginOauth) {
		this.haveLoginOauth = haveLoginOauth;
	}
	public boolean isHaveAdmin() {
		return haveAdmin;
	}
	public void setHaveAdmin(boolean haveAdmin) {
		this.haveAdmin = haveAdmin;
	}
	public boolean isHaveUser() {
		return haveUser;
	}
	public void setHaveUser(boolean haveUser) {
		this.haveUser = haveUser;
	}
	
}
