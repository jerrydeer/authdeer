package com.anuparp.deeruser.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.anuparp.deeruser.entities.SysUserEntity;

@Repository
public interface SysUserRepository extends JpaRepository<SysUserEntity, String> {
	
	public List<SysUserEntity> findAll();
	
	public List<SysUserEntity> findByUsernameAndPassword(String username, String password);
}
