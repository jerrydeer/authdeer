package com.anuparp.deeruser.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.anuparp.deeruser.entities.SysUserEntity;
import com.anuparp.deeruser.repositories.SysUserRepository;

@Service
public class SysUserService {

	@Autowired
	private SysUserRepository sysUserRepo;

	public Optional<SysUserEntity> getUserEntityById(String username) {
		Optional<SysUserEntity> sysUserEntity = sysUserRepo.findById(username);
		return sysUserEntity;
	}

	public SysUserEntity getUserByUsernamePassword(String username, String password) {
		SysUserEntity response = null;
		List<SysUserEntity> findByUsernameAndPassword = sysUserRepo.findByUsernameAndPassword(username, password);
		if (findByUsernameAndPassword != null && findByUsernameAndPassword.size() > 0) {
			response = findByUsernameAndPassword.get(0);
		}
		return response;
	}

}
