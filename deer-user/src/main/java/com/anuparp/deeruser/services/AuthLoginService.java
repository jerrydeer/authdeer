package com.anuparp.deeruser.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.anuparp.deeruser.entities.SysUserEntity;
import com.anuparp.deeruser.model.UsernamePasswordDTO;

@Service
public class AuthLoginService {

	@Autowired
	private SysUserService userService;

	public UsernamePasswordDTO verifyUsernamePassword(String username, String password) throws Exception {
		UsernamePasswordDTO response = new UsernamePasswordDTO();
		SysUserEntity userEntity = userService.getUserByUsernamePassword(username, password);
		if (userEntity != null) {
			response.setUsername(userEntity.getUsername());
		}
		return response;
	}
	
}
