package com.anuparp.deeruser.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.anuparp.deeruser.entities.SysUserEntity;
import com.anuparp.deeruser.exception.UnauthorizedException;
import com.anuparp.deeruser.model.ResponseAccessTokenModel;
import com.anuparp.deeruser.model.UserProfileModel;
import com.anuparp.deeruser.model.UserRoles;

@Service
public class UserProfileService {

	@Autowired
	private SysUserService userService;
	@Autowired
	private CallApiDeerAuth callApiDeerAuth;

	public ResponseAccessTokenModel verifyAndRefreshToken(String reqAccessToken) {
		ResponseAccessTokenModel verifyAndRefreshToken = callApiDeerAuth.verifyAndRefreshToken(reqAccessToken);
		return verifyAndRefreshToken;
	}
	
	public UserProfileModel getUserProfileByID(String username) throws Exception {
		if (username == null) {
			throw new UnauthorizedException("token not found");
		}
		Optional<SysUserEntity> sysUserEntityOptional = userService.getUserEntityById(username);
		if (sysUserEntityOptional.isEmpty()) {
			throw new UnauthorizedException("user not found");
		}
		SysUserEntity sysUserEntity = sysUserEntityOptional.get();
		UserProfileModel userProfileModel = new UserProfileModel();
		userProfileModel.setUsername(sysUserEntity.getUsername());
		userProfileModel.setFirstname(sysUserEntity.getFirstname());
		userProfileModel.setSurname(sysUserEntity.getSurname());
		userProfileModel.setEmail(sysUserEntity.getEmail());
		userProfileModel.setUserStatus(sysUserEntity.getUserStatus());
		
		userProfileModel.setUserRoles(getUserRolesMock());
		return userProfileModel;
	}
	
	// ------------------------------------------------------------------------------------------
	private List<UserRoles> getUserRolesMock() {
		List<UserRoles> listUserRoles = new ArrayList<>();
		{
			UserRoles userRoles = new UserRoles();
			userRoles.setRoleId(1);
			userRoles.setRoleName("admin");
			listUserRoles.add(userRoles);
		}
		{
			UserRoles userRoles = new UserRoles();
			userRoles.setRoleId(2);
			userRoles.setRoleName("checker");
			listUserRoles.add(userRoles);
		}
		return listUserRoles;
	}
	
}
