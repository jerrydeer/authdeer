package com.anuparp.deeruser.services;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.anuparp.deeruser.exception.InternalServerErrorException;
import com.anuparp.deeruser.model.ResponseAccessTokenModel;

@Service
public class CallApiDeerAuth {

	public ResponseAccessTokenModel verifyAndRefreshToken(String token) {
		String url = "http://localhost:8088/auth-deer/api/oauth2/access-token/verifyAndRefreshToken";
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			httpHeaders.set("Authorization", "Bearer " + token);
			HttpEntity<Object> request = new HttpEntity<>(httpHeaders);
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<ResponseAccessTokenModel> response = restTemplate.postForEntity(url, request,
					ResponseAccessTokenModel.class);
			return response.getBody();
		} catch (Exception e) {
			throw new InternalServerErrorException("Call API failed " + e.getMessage() + " @ " + url);
		}
	}

	public ResponseAccessTokenModel validateTokenExpires(String reqAccessToken) {
		ResponseAccessTokenModel res = new ResponseAccessTokenModel();
		res.setAccess_token(reqAccessToken);
		return res;
	}

	public ResponseAccessTokenModel refreshToken(String oldToken) {
		ResponseAccessTokenModel res = new ResponseAccessTokenModel();
		res.setAccess_token(oldToken);
		return res;
	}
}
