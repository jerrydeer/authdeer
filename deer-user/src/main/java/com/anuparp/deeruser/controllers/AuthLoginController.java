package com.anuparp.deeruser.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.anuparp.deeruser.exception.InternalServerErrorException;
import com.anuparp.deeruser.model.UsernamePasswordDTO;
import com.anuparp.deeruser.services.AuthLoginService;

@RestController
@RequestMapping("/api/oauth/login")
public class AuthLoginController {

	@Autowired
	private AuthLoginService authLoginService;

	@PostMapping("/validateCredentialUser")
	public ResponseEntity<UsernamePasswordDTO> validateCredential(@RequestBody UsernamePasswordDTO reqBody) {
		try {
			UsernamePasswordDTO response = authLoginService.verifyUsernamePassword(reqBody.getUsername(), reqBody.getPassword());
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			throw new InternalServerErrorException(e.getMessage());
		}
	}
	
}
