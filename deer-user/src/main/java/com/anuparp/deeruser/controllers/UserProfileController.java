package com.anuparp.deeruser.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.anuparp.deeruser.exception.InternalServerErrorException;
import com.anuparp.deeruser.exception.UnauthorizedException;
import com.anuparp.deeruser.exception.UnprocessableEntityException;
import com.anuparp.deeruser.model.ResponseAccessTokenModel;
import com.anuparp.deeruser.model.ResponseModel;
import com.anuparp.deeruser.model.UserProfileModel;
import com.anuparp.deeruser.services.UserProfileService;

@RestController
@RequestMapping("/api/user")
public class UserProfileController {

	@Autowired
	private UserProfileService userProfileService;

	@GetMapping("/getUserProfile")
	public ResponseEntity<ResponseModel<UserProfileModel>> getUserProfile(@RequestHeader HttpHeaders headers) {
		try {
			ResponseAccessTokenModel validatedToken = validateRequestHeaderAuthentication(headers);

			UserProfileModel userProfile = userProfileService.getUserProfileByID(validatedToken.getCreate_by());

			return new ResponseEntity<>(buildUserProfileResponse(userProfile),
					buildHeaderResponse(validatedToken.getAccess_token()), HttpStatus.OK);
		} catch (UnprocessableEntityException e) {
			throw new UnprocessableEntityException(e.getMessage());
		} catch (UnauthorizedException e) {
			throw new UnauthorizedException(e.getMessage());
		} catch (Exception e) {
			throw new InternalServerErrorException(e.getMessage());
		}
	}

	// ---------------------------------------------------------------------------------------
	private ResponseAccessTokenModel validateRequestHeaderAuthentication(HttpHeaders reqHeader) throws Exception {
		try {
			String authorizationRequestHeader = reqHeader.getFirst(HttpHeaders.AUTHORIZATION);
			if (null == authorizationRequestHeader || "".equals(authorizationRequestHeader)) {
				throw new UnprocessableEntityException("missing Authorization request header");
			} // token = [0]Bearer [1]token
			String reqAccessToken = authorizationRequestHeader.split(" ")[1];

			ResponseAccessTokenModel validatedToken = userProfileService.verifyAndRefreshToken(reqAccessToken);
			return validatedToken;
		} catch (UnprocessableEntityException e) {
			throw new UnprocessableEntityException(e.getMessage());
		} catch (Exception e) {
			throw new InternalServerErrorException(e.getMessage());
		}
	}

	private ResponseModel<UserProfileModel> buildUserProfileResponse(UserProfileModel userProfileByToken) {
		ResponseModel<UserProfileModel> response = new ResponseModel<>();
		response.setBody(userProfileByToken);
		response.setMessage("SUCCESS");
		return response;
	}

	private HttpHeaders buildHeaderResponse(String token) {
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("Authorization", "Bearer " + token);
		return responseHeaders;
	}

}
