package com.anuparp.deeruser.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.anuparp.deeruser.entities.SysUserEntity;
import com.anuparp.deeruser.repositories.SysUserRepository;

@RestController
@RequestMapping("/api/admin")
public class AdminSysUserController {

    @Autowired
    private SysUserRepository repo;

    @GetMapping("/listSysUser")
    public List<SysUserEntity> listSysUser(){
    	return repo.findAll();
    }
    
    @GetMapping("/getUserProfileByLogin")
    public List<SysUserEntity> getUserProfileByLogin(@RequestBody SysUserEntity reqBody) {
    	List<SysUserEntity> findByIdAndPassword = repo.findByUsernameAndPassword(reqBody.getUsername(), reqBody.getPassword());
    	return findByIdAndPassword;
    }
    
    @PostMapping("/addUser")
    public SysUserEntity addUser(@RequestBody SysUserEntity reqBody) {
    	SysUserEntity result = null;
    	try {
    		result = repo.save(reqBody);
    	}catch(Exception e) {
    		System.out.println(e);
    	}
		return result;
    }
    
    @DeleteMapping("/deleteUser")
    public Integer deleteUser(@RequestBody SysUserEntity reqBody) {
    	Integer result = 0;
    	try {
    		repo.delete(reqBody);
    		result = 1;
    	}catch(Exception e) {
    		result = null;
    	}
    	return result;
    }
    
}
