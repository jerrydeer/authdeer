package com.anuparp.deeruser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeerUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeerUserApplication.class, args);
	}

}
